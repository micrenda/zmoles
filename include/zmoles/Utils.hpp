#pragma once

#include <regex>
#include <string>

namespace dfpe
{
    class Utils
    {
    public:
        static const std::string formatElementPretty(const std::smatch &what);
        static const std::string formatElementLatex(const std::smatch &what);
        static const std::string getScriptedNumber(int number, bool superscript, bool forcePlus = false);

    public:
        static const std::regex specieRegex;
        static const std::regex moleculeRegex;
        static const std::regex elementRegex;
        static const std::regex isomerRegex;


    };
}

