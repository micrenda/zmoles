#pragma once

#include <string>
#include <ostream>

#include "Molecule.hpp"
#include "PrintMode.hpp"
#include "ParticleSpecie.hpp"

namespace dfpe
{
    class IonSpecie
    {
    public:
        IonSpecie(const IonSpecie& other) :
                molecule(other.molecule), ionization(other.ionization), state(other.state) {}
                
        IonSpecie(const Molecule &molecule, int ionization = 0, const std::string &state = "") :
                molecule(molecule), ionization(ionization), state(state) {}

        QtySiMass              	getMass()       const;
        QtySiElectricCharge		getCharge() 	const;
        int						getIonization()	const;
        const std::string		getState()  	const;

	public:
        const std::string toString(const PrintMode& mode = PrintMode::BASIC)  const;
        
    protected:    
        const std::string toBasicString()  const;
        const std::string toPrettyString() const;
        const std::string toLatexString()  const;
        
    public:
		IonSpecie& operator=(const IonSpecie &rhs);
        bool operator==(const IonSpecie &rhs) const;
        bool operator!=(const IonSpecie &rhs) const;
        bool similar(const IonSpecie &rhs, bool allowStateMismatch = false, bool allowIonizationMismatch = false) const;
      
    public:
        IonSpecie& operator+=(const IonSpecie& rhs);
        
        IonSpecie& operator+=(const ParticleSpecie& rhs);
        IonSpecie& operator-=(const ParticleSpecie& rhs);
        
        friend IonSpecie operator+(const IonSpecie& lhs, const IonSpecie& rhs);
        friend IonSpecie operator+(const IonSpecie& lhs, const ParticleSpecie& rhs);
        friend IonSpecie operator+(const ParticleSpecie& lhs, const IonSpecie& rhs);
        friend IonSpecie operator-(const IonSpecie& lhs, const ParticleSpecie& rhs);

    protected:
        Molecule 		molecule;
        int				ionization;
        std::string 	state;
    public:
        const Molecule &getMolecule() const { return molecule; }

		bool operator<(const IonSpecie &rhs) const;
		
        friend std::ostream &operator<<(std::ostream &os, const IonSpecie &specie);


    };
    
    IonSpecie operator+(const IonSpecie& lhs, const IonSpecie& rhs);
	IonSpecie operator+(const IonSpecie& lhs, const ParticleSpecie& rhs);
	IonSpecie operator+(const ParticleSpecie& lhs, const IonSpecie& rhs);
	IonSpecie operator-(const IonSpecie& lhs, const ParticleSpecie& rhs);

}

