#pragma once

#include <stdexcept>
#include <string>
#include <sstream>
#include <vector>

namespace dfpe
{
	class ZMolesErrorCls : public std::runtime_error
	{
	private:\
		std::string msg;

	public:
        ZMolesErrorCls(const std::string &arg, const char *file, int line):
			std::runtime_error(arg)
		{
			msg = std::string(file) + ":" + std::to_string(line) + ": " + std::string(arg);
		}

        ZMolesErrorCls(const std::string &arg, const std::exception& parent, const char *file, int line):
            ZMolesErrorCls(arg, file, line)
		{
		    msg += "\n" + std::string(parent.what());
		}
		
		~ZMolesErrorCls() throw() {}

        const std::string &get_msg() const {return msg;}
        const char *what() const throw() { return msg.c_str();}
	};
	
	typedef ZMolesErrorCls ZMolesError;
}

#define ZMolesError(arg)            dfpe::ZMolesErrorCls(arg, __FILE__, __LINE__);
#define ZMolesErrorFwd(arg, parent) dfpe::ZMolesErrorCls(arg, parent, __FILE__, __LINE__);
