#pragma once

#include <string>
#include <array>
#include <optional>

#include "PrintMode.hpp"
#include "ElectronicExcitation.hpp"
#include "VibrationalExcitation.hpp"
#include "RotationalExcitation.hpp"
#include "ZMolesError.hpp"

namespace dfpe
{
    class Molecule;

    class MoleculeState
    {
    public:
        explicit MoleculeState(const Molecule *molecule) : molecule(molecule) {}

    protected:
        const Molecule* molecule;

        std::optional<ElectronicExcitation>  electronicExcitation;
        std::optional<VibrationalExcitation> vibrationalExcitation;
        std::optional<RotationalExcitation>  rotationalExcitation;

    public:
        bool isGroundState() const;

        std::string toString(bool compact = true, const PrintMode& mode = PrintMode::BASIC ) const;

        int getDegeneracy() const
        {
            throw ZMolesError("Not implemented");
        }

    };
}

