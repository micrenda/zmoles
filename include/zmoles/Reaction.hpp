#pragma once

#include <string>
#include <vector>
#include <ostream>
#include <random>
#include <optional>

#include "Specie.hpp"
#include "Products.hpp"
#include "Reactants.hpp"

namespace dfpe
{
    class Reaction
    {
    public:
         Reaction();
        ~Reaction();

    protected:
        Reactants reactants;
        std::vector<Products> products;
        std::vector<std::optional<QtySiDimensionless>> productsWeights;
        mutable std::variant<std::uniform_int_distribution<int>,std::discrete_distribution<int>> productsDistribution;
    public:

        const std::string toString         (const PrintMode& mode = PrintMode::BASIC) const;

        void beautify();

        const Reactants &getReactants() const{return reactants;}
        const std::vector<Products> &getProducts() const {return products;}

        void setBullet(const Specie& bullet)                 { Reaction::reactants.setBullet(bullet); }
        void setTarget(const Specie& target)                 { Reaction::reactants.setTarget(target); }
        std::optional<QtySiDimensionless> getProductWeight(unsigned int channel) const {return productsWeights.at(channel);};
        void addProduct(unsigned int channel, const Specie& product, std::optional<QtySiDimensionless> weight);


        bool hasMassConservation()    const;
        bool hasChargeConservation()  const;

        bool hasProducts() const;
        bool hasWeightedProducts() const;

        void completeProductFragments();
        
        void generateProductsElastic();
        void generateProductsIonization();
        void generateProductsAttachment();
/*
        bool operator<(const Reaction &rhs) const;
        bool operator>(const Reaction &rhs) const;
        bool operator<=(const Reaction &rhs) const;
        bool operator>=(const Reaction &rhs) const;
*/

        friend std::ostream  &operator<<(std::ostream &os,  const Reaction &reaction);
        //friend std::wostream &operator<<(std::wostream &os, const Reaction &reaction);

        const std::vector<std::optional<QtySiDimensionless>> &getProductsWeights() const {return productsWeights;}

        std::variant<std::uniform_int_distribution<int>, std::discrete_distribution<int>> &getProductsDistribution() const{return productsDistribution;}

        void setProductsWeight(unsigned int channel, std::optional<QtySiDimensionless> &weight);
        std::optional<QtySiDimensionless> getProductsWeight(unsigned int channel) const;
    };

    std::ostream  &operator<<(std::ostream &os,  const Reaction &reaction);
    //std::wostream &operator<<(std::wostream &os, const Reaction &reaction);
}


