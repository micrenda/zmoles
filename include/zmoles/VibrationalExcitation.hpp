#pragma once

#include <optional>
#include "BaseExcitation.hpp"
#include "VibrationalModeType.hpp"

namespace dfpe
{

    class Molecule;

    class VibrationalExcitation: public BaseExcitation
    {
    public:
        explicit VibrationalExcitation(const Molecule *molecule) : BaseExcitation(molecule) {}
        VibrationalExcitation(const Molecule *molecule, int v) : BaseExcitation(molecule), v(v) {}

        int getV() const;

        bool isGround() const override;
        void validate() const override;

        std::string toString(bool compact, const PrintMode &printMode) const override;

    protected:
        int v = 0;
        std::optional<VibrationalModeType> mode;
    };

} // dfpe