#pragma once

#include <string>
#include "PrintMode.hpp"

namespace dfpe
{
    enum class VibrationalModeType
    {
        SYMMETRICAL_STRETCHING,
        ASYMMETRICAL_STRETCHING,
        SCISSORING,
        ROCKING,
        WAGGING,
        TWISTING,
        OUT_OF_PLANE
    };

    class VibrationalModeTypeUtil
    {
    public:
        static const std::string getName(const VibrationalModeType &type);
        static const std::string getSymb(const VibrationalModeType &type, PrintMode mode);
        static const std::string getDesc(const VibrationalModeType &type);
        static VibrationalModeType parse(const std::string &s);
    };

} // dfpe

