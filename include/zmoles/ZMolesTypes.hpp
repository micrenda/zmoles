#pragma once

#include <boost/units/dimension.hpp>
#include <boost/units/base_dimension.hpp>
#include <boost/units/derived_dimension.hpp>
#include <boost/units/base_unit.hpp>

#include <boost/units/systems/si/length.hpp>
#include <boost/units/systems/si/time.hpp>
#include <boost/units/make_system.hpp>
#include <boost/units/io.hpp>

#include <qtydef/QtyDefinitions.hpp>

namespace dfpe
{

    /// derived dimension for rotational inertia in SI units : M L²
    typedef boost::units::make_dimension_list<boost::mpl::list<
            boost::units::dim<boost::units::mass_base_dimension,    boost::units::static_rational<1>>,
            boost::units::dim<boost::units::length_base_dimension,    boost::units::static_rational<2>>
    >>::type     rotational_inertia_dimension;
	
	typedef boost::units::unit<rotational_inertia_dimension, boost::units::si::system>      si_rotational_inertia;

	typedef boost::units::quantity<si_rotational_inertia>  QtySiRotationalInertia;

}
