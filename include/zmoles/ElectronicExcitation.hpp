#pragma once

#include "BaseExcitation.hpp"
#include <boost/rational.hpp>
#include <optional>

namespace dfpe
{
    enum class ParityInversionType
    {
        EVEN = 1,
        ODD = -1
    };

    enum class ReflectionSymmetryType
    {
        SYMMETRIC = 1,
        ANTISYMMETRIC = -1
    };



    class ElectronicExcitation: public BaseExcitation
    {
    public:
        explicit ElectronicExcitation(const Molecule *molecule);

        bool isGround() const override;
        void validate() const override;

        std::string toString(bool compact, const PrintMode &mode) const override;

        int getMultiplicity() const;

        bool isSinglet() const;
        bool isDoublet() const;
        bool isTriplet() const;
        bool isQuartet() const;
        bool isQuintet() const;

    protected:

        /// Alternative empirical notation for electronic states, ordered by energy. X is the ground state, A the first one, etc.
        int  ordinal = 0;
        /// In the alternative empirical notation, it indicates if the total spin quantum number is the same as the ground state
        bool ordinalSpinMatch = true;

        /// total spin quantum number (S for atomic and molecular excitation)
        boost::rational<int> s = 0;

        /// orbital angular momentum (l for atomic excitation, Λ for molecular excitation)
        int l = 0;

        /// total orbital quantum number (J for atomic excitation, Ω for molecular excitation)
        boost::rational<int> j = 0;

        // Inversion symmetry
        std::optional<ParityInversionType> parity;
        // Reflection symmetry
        std::optional<ReflectionSymmetryType> reflection;
    };

} // dfpe
