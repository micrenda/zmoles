#pragma once

#include "PrintMode.hpp"

namespace dfpe
{

    class Molecule;

    class BaseExcitation
    {
    public:
        explicit BaseExcitation(const Molecule *molecule);

        virtual bool isGround() const = 0;
        virtual void validate() const = 0;
        virtual std::string toString(bool compact, const PrintMode &mode) const = 0;

    protected:
        const Molecule *molecule;
    };

} // dfpe
