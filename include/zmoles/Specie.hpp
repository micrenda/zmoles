#pragma once
#include <string>
#include "ParticleSpecie.hpp"
#include "IonSpecie.hpp"
#include <variant>
#include <ostream>

namespace dfpe
{
	class Molecule;

	class Specie
	{
	public:
        Specie() = default;
        Specie(const ParticleSpecie& particle): content(particle) {};
        Specie(const IonSpecie&      ion): 		content(ion) {};
		explicit Specie(const std::string&  formula);

		~Specie(){};
		
	protected:
		std::variant<std::monostate, ParticleSpecie, IonSpecie> content;
		
	public:
	    bool isParticle() const;
        bool isIon()     const;
        
        const ParticleSpecie&  getParticle() const;
        const IonSpecie& 	   getIon()      const;

	public:
		
		QtySiMass 		    getMass() const;
		QtySiElectricCharge getCharge() const;
		
		int getIonization() const;
		
		//Specie& operator=(const Specie &rhs);
        bool operator==(const Specie &rhs) const;
        bool operator!=(const Specie &rhs) const;
		
		const std::string toString(const PrintMode& mode = PrintMode::BASIC)  const;
		friend std::ostream &operator<<(std::ostream &os, const Specie &specie);
		
		
		// overloaded < operator
		bool operator <(const Specie& other) const;
		//static friend bool equalsExceptState(const Specie &lhs, const Specie &rhs);
	};

    std::ostream &operator<<(std::ostream &os, const Specie &specie);
    
    
    class SimilarPredicate
    { 
	   public: 
	   SimilarPredicate(const Specie& specie, bool allowStateMismatch = true, bool allowIonizationMismatch  = true):
				specie(specie), allowStateMismatch(allowStateMismatch), allowIonizationMismatch(allowIonizationMismatch)  {};
	   
	   public:
       bool operator()(const Specie &other) const
       {
		   	if (specie.isParticle() && other.isParticle())
				return specie.getParticle() == other.getParticle();
			else if (specie.isIon() && other.isIon())
				return specie.getIon().similar(other.getIon(), true, true);
			else
				return false;
	   };
	   
	   private:
	   
	   const Specie specie;
	   
	   bool allowStateMismatch;
	   bool allowIonizationMismatch;
	   
	   
	}; 

}


