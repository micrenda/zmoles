#pragma once

#include "BaseExcitation.hpp"

namespace dfpe
{

    class Molecule;

    class RotationalExcitation: public BaseExcitation
    {
    public:
        RotationalExcitation(const Molecule *molecule);
        RotationalExcitation(const Molecule *molecule, int j);
        RotationalExcitation(const Molecule *molecule, int j, int ka);
        RotationalExcitation(const Molecule *molecule, int j, int ka, int kc);

        int getJ() const;
        int getKa() const;
        int getKc() const;

        bool isGround() const override;
        void validate() const override;

        std::string toString(bool compact, const PrintMode &mode) const override;

    protected:

        int j  = 0;
        int ka = 0;
        int kc = 0;
    };

} // dfpe
