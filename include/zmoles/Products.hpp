#pragma once

#include <optional>
#include <vector>
#include "Specie.hpp"

namespace dfpe
{
    class Products
    {

    protected:
        std::vector<Specie> items;

    public:
        bool empty() const;

        void clear();

        const std::vector<Specie> &getItems() const { return items; }

        void setItems(const std::vector<Specie> &items) { Products::items = items; }

        void append(const dfpe::Specie &specie);

        const std::string toString(const PrintMode &mode) const;

        int getElectronsCount() const;

        int getIonsCount() const;

        int getSpecieCount(const Specie specie, bool ignoreCharge, bool ignoreState) const;

    };

}


