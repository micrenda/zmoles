#pragma once

#include <optional>
#include <map>
#include "ParticleSpecie.hpp"
#include <filesystem>
#include <regex>

namespace dfpe
{
    class PDGTable
    {
    public:
        PDGTable();

        std::optional<ParticleSpecie> get(const std::string& name) const;
        std::optional<ParticleSpecie> get(int id) const;
        
    protected:
        void load(const std::filesystem::path& filename);
        std::regex pdgFilenameRegex = std::regex("mass_width_(\\d+).mcd");

        std::map<int, ParticleSpecie> content;

    public:
        static const PDGTable instance;

    };

}
