#pragma once

#include <boost/units/systems/si/codata/physico-chemical_constants.hpp>
#include <variant>
#include <map>
#include <string>
#include <vector>
#include <ostream>
#include <optional>

#include "Element.hpp"
#include "PrintMode.hpp"
#include "Hashes.hpp"
#include "MoleculeComponent.hpp"
#include "ZMolesTypes.hpp"
#include "Shape.hpp"

namespace dfpe
{

    class MoleculeComponent;

    class Molecule
    {
    public:
        Molecule(const Molecule&    other);
        Molecule(const Element& element, unsigned int quantity);
        explicit Molecule(const std::string& formula);

    protected:

        std::vector<MoleculeComponent> components;
        std::vector<unsigned int>      quantities;
        std::optional<std::string>     isomer;

        QtySiMass cachedMass;
        bool      cachedPolar = false;
        size_t    cachedHash = 0;

    public:
        const QtySiMass &getMass() const;

        const std::optional<std::string> &getIsomer() const{return isomer;}
        void setIsomer(const std::optional<std::string> &value) {Molecule::isomer = value;}

        const MoleculeComponent& getComponent(unsigned int component) const;
        unsigned int getQuantity(unsigned int component) const;

        unsigned int getElementQuantity(const Element& element) const;
        unsigned int getSize() const;

        const std::string getFormula() const;

        const QtySiEnergy getRotationalEnergy(int j, QtySiRotationalInertia inertia) const;

        Molecule compact();

        bool isMonoatomic() const;
        bool isDiatomic() const;

        bool isHomonuclear() const;
        bool isHeteronuclear() const;
        bool isIsomer() const;
        bool isPolar() const;

        //TODO: calculate the molecule shape using VSEPR theory
        Shape getShape() const;
        std::pair<Shape,std::vector<QtySiPlaneAngle>> getShapeWithAngles() const;

        //TODO: calculate the electrons geometry using VSEPR theory
        Shape getElectronsGeometry() const;

    public:
        const std::string toString(const PrintMode& mode = PrintMode::BASIC) const;
    
    protected:
        const std::string toBasicString()  const;
        const std::string toPrettyString() const;
        const std::string toLatexString()  const;

    public:
        bool operator==(const Molecule &rhs) const;
        bool operator!=(const Molecule &rhs) const;
        
    public:

        Molecule& operator=(const Molecule& rhs);
        Molecule& operator+=(const Molecule& rhs);
        Molecule& operator-=(const Molecule& rhs);

        friend Molecule operator+(const Molecule& lhs, const Molecule& rhs);
        friend Molecule operator-(const Molecule& lhs, const Molecule& rhs);

    protected:
        void updateCache();
        bool guessPolarity() const;

    public:
        friend std::ostream &operator<<(std::ostream &os, const Molecule &molecule);
        bool operator<(const Molecule &rhs) const;
    };

    Molecule operator+(const Molecule& lhs, const Molecule& rhs);
    Molecule operator-(const Molecule& lhs, const Molecule& rhs);

    std::ostream &operator<<(std::ostream &os, const Molecule &molecule);
    
    
}




