#pragma once

namespace dfpe
{
    enum class  Shape
    {
        UNKNOWN,

        LINEAR,
        BENT,

        TRIGONAL_PLANAR,
        TRIGONAL_PYRAMIDAL,
        TRIGONAL_BIPYRAMIDAL,

        SQUARE_PLANAR,
        SQUARE_PYRAMIDAL,

        SEE_SAW,
        T_SHAPE,

        TETRAHEDRAL,

        OCTAHEDRAL
    };
}