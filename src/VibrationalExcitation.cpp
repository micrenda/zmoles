//
// Created by mrenda on 5/3/23.
//

#include <sstream>
#include "zmoles/VibrationalExcitation.hpp"

namespace dfpe
{

    int VibrationalExcitation::getV() const
    {
        return v;
    }

    bool VibrationalExcitation::isGround() const
    {
        return v == 0;
    }

    void VibrationalExcitation::validate() const
    {

    }

    std::string VibrationalExcitation::toString(bool compact, const PrintMode &printMode) const
    {
        std::stringstream ss;
        if (!isGround() || mode.has_value() || !compact)
        {
            if (ss.tellp() > 0) [[unlikely]] ss << ", ";
            ss << "v=" << v;
        }

        if (mode.has_value())
        {
            if (ss.tellp() > 0) [[likely]] ss << ", ";
            ss << VibrationalModeTypeUtil::getSymb( *mode, printMode );
        }

        return ss.str();
    }
} // dfpe