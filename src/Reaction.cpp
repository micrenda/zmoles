#include <zmoles/ParticleSpecie.hpp>
#include <boost/units/quantity.hpp>
#include <boost/units/cmath.hpp>
#include <boost/units/systems/detail/constants.hpp>
#include <boost/units/systems/si/codata/electromagnetic_constants.hpp>
#include <boost/units/systems/si/codata/physico-chemical_constants.hpp>
#include <boost/units/systems/si/electric_charge.hpp>
#include <boost/units/systems/si/mass.hpp>
#include <boost/units/unit.hpp>
#include <ostream>

#include "zmoles/IonSpecie.hpp"
#include "zmoles/Specie.hpp"
#include "zmoles/Reaction.hpp"
#include <iostream>
#include <zmoles/ElectronSpecie.hpp>

using namespace std;
using namespace dfpe;
using namespace boost::units;

Reaction::Reaction()
{
}

Reaction::~Reaction()
{

}







const string Reaction::toString(const PrintMode &mode) const
{

    stringstream ss;

    ss << reactants.toString(mode);

    if (mode == PrintMode::BASIC)
        ss << " -> ";
    else if (mode == PrintMode::PRETTY)
        ss << " → ";
    else if (mode == PrintMode::LATEX)
        ss << " \\rightarrow ";

    bool first = true;
    for (const Products& product: products)
    {
        if (!first)
        {
            if (mode == PrintMode::BASIC)
                ss << " or ";
            else if (mode == PrintMode::PRETTY)
                ss << " or ";
            else if (mode == PrintMode::LATEX)
                ss << " \\text{or} ";
        }
        ss << product.toString(mode);
        first = false;
    }

    return ss.str();
}

bool Reaction::hasProducts() const
{
    return !products.empty();
}

bool Reaction::hasMassConservation() const
{
    QtySiMass reactantsMass;
    reactantsMass += reactants.getBullet().getMass() + reactants.getTarget().getMass();

    bool massConvervation = !products.empty();
    for (const auto& product: products)
    {
        QtySiMass         productsMass;
        for (const Specie &specie: product.getItems())
            productsMass += specie.getMass();
       massConvervation &= boost::units::abs(productsMass - reactantsMass) < si::constants::codata::m_u * 1e-10;
    }

    return massConvervation;
}

bool Reaction::hasChargeConservation() const
{
    QtySiElectricCharge reactantsCharge = reactants.getBullet().getCharge() + reactants.getTarget().getCharge();

    bool chargeConvervation = !products.empty();

    for (const auto& product: products)
    {
        QtySiElectricCharge productsCharge;
        for (const Specie &specie: product.getItems())
            productsCharge += specie.getCharge();
        chargeConvervation &= abs(productsCharge - reactantsCharge) < si::constants::codata::e * 1e-10;
    }

    return chargeConvervation;
}

void Reaction::completeProductFragments()
{
    if (reactants.getIonsCount() != 0)
    {
        Molecule sumReactants("");

        if (reactants.getBullet().isIon())
            sumReactants += reactants.getBullet().getIon().getMolecule();

        if (reactants.getTarget().isIon())
            sumReactants += reactants.getTarget().getIon().getMolecule();

        for (Products &product: products)
        {
            if (product.getIonsCount() != 0)
            {
                Molecule sumProducts("");

                for (const Specie &specie: product.getItems())
                {
                    if (specie.isIon())
                        sumProducts += specie.getIon().getMolecule();
                }

                sumReactants = sumReactants.compact();
                sumProducts  = sumProducts.compact();

                if (sumReactants != sumProducts)
                {
                    Molecule diff = (sumReactants - sumProducts).compact();

                    vector<Specie> species = product.getItems();
                    species.push_back(IonSpecie(diff));
                    product.setItems(species);
                }
            }
        }
    }
}

void Reaction::generateProductsElastic()
{
    products.clear();
    Products product;
    product.append(reactants.getBullet());
    product.append(reactants.getTarget());
    products.push_back(product);
}

void Reaction::generateProductsIonization()
{
    products.clear();
    Products product;

	Specie bullet = reactants.getBullet();
	Specie target = reactants.getTarget();
	
	ElectronSpecie electron;
	
	if (bullet.isParticle())
	{
		product.append(Specie(target.getIon() - bullet.getParticle()));
		product.append(bullet);
		product.append(Specie(electron));
	}
	else if (bullet.isIon())
	{
		product.append(Specie(target.getIon() - electron));
		product.append(bullet);
		product.append(Specie(electron));
	}

    products.push_back(product);
}


void Reaction::generateProductsAttachment()
{
	products.clear();

	Products product;

	Specie bullet = reactants.getBullet();
	Specie target = reactants.getTarget();
	
	if (bullet.isParticle())
	{
		product.append(IonSpecie(target.getIon().getMolecule(), target.getIon().getIonization() - 1, target.getIon().getState()));
	}
	else if (bullet.isIon())
	{
	    product.append(IonSpecie(target.getIon().getMolecule() + bullet.getIon().getMolecule(), target.getIon().getIonization() + bullet.getIon().getIonization(), ""));
	}
	products.push_back(product);
}

ostream &dfpe::operator<<(ostream &os, const Reaction &reaction)
{
    os << reaction.toString(PrintMode::BASIC);
    return os;
}
/*
vector<Specie> Reaction::getCreatedProducts() const
{
    vector<Specie> result = products;

    for (const Specie& reactant: reactants)
    {
        auto common = find(result.begin(), result.end(), reactant);

        if (common != result.end())
            result.erase(common);
    }

    return result;
}


vector<Specie> Reaction::getDestroyedReactants() const
{
    vector<Specie> result = reactants;

    for (const Specie& product: products)
    {
        auto common = find(result.begin(), result.end(), product);

        if (common != result.end())
            result.erase(common);
    }

    return result;
}
*/


void Reaction::addProduct(unsigned int channel, const Specie& product, std::optional<QtySiDimensionless> weight)
{
    if (products.size() <= channel)
        products.resize(channel + 1);
    products[channel].append(product);

    setProductsWeight(channel, weight);
}

void Reaction::setProductsWeight(unsigned int channel, optional<QtySiDimensionless> &weight)
{
    if (productsWeights.size() <= channel)
        productsWeights.resize(channel + 1);
    productsWeights[channel] = weight;

    if (hasWeightedProducts())
    {
        vector<double> weights;
        for (const auto& weight: productsWeights)
            weights.push_back(weight->value());

        productsDistribution = discrete_distribution<int>(weights.begin(), weights.end());
    }
    else
        productsDistribution = uniform_int_distribution<int>(0, productsWeights.size() - 1);

}

std::optional<QtySiDimensionless> Reaction::getProductsWeight(unsigned int channel) const
{
    return productsWeights.at(channel);
}

bool Reaction::hasWeightedProducts() const
{
    for (const auto& weight: productsWeights)
    {
        if (!weight.has_value())
            return false;
    }
    return true;
}

void Reaction::beautify()
{
}

/*
bool Reaction::operator<(const Reaction &rhs) const {
	if (getBullet() < rhs.getBullet())
		return true;
	if (rhs.getBullet() < getBullet())
		return false;
	return getTarget() < rhs.getTarget();
}

bool Reaction::operator>(const Reaction &rhs) const {
	return rhs < *this;
}

bool Reaction::operator<=(const Reaction &rhs) const {
	return !(rhs < *this);
}

bool Reaction::operator>=(const Reaction &rhs) const {
	return !(*this < rhs);
}
*/


