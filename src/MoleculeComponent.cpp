#include "zmoles/Molecule.hpp"
#include "zmoles/MoleculeComponent.hpp"



using namespace dfpe;
using namespace std;

bool MoleculeComponent::isElement() const
{
    return simple;
}

bool MoleculeComponent::isMolecule() const
{
    return !simple;
}

Element MoleculeComponent::getElement() const
{
   return *(Element*)component;
}

Molecule MoleculeComponent::getMolecule() const
{
    return *(Molecule*)component;
}


MoleculeComponent::MoleculeComponent(const MoleculeComponent &other) {
    if (other.isElement())
    {
        component = new Element(other.getElement());
        simple = true;
    }
    else if (other.isMolecule())
    {
        component = new Molecule(other.getMolecule());
        simple = false;
    }
}


MoleculeComponent::MoleculeComponent(const Element &element)
{
    component = new Element(element);
    simple = true;
}

MoleculeComponent::MoleculeComponent(const Molecule &molecule)
{
    component = new Molecule(molecule);
    simple = false;
}

MoleculeComponent::~MoleculeComponent()
{
    if (component != nullptr)
    {
        if (simple)
            delete (Element*) component;
        else
            delete (Molecule*) component;

        component = nullptr;
    }
}

bool MoleculeComponent::operator==(const MoleculeComponent &rhs) const {
    if (isElement() && rhs.isElement())
        return getElement() == rhs.getElement();
    else if (isMolecule() && rhs.isMolecule())
        return getMolecule() == rhs.getMolecule();

    return false;
}

bool MoleculeComponent::operator!=(const MoleculeComponent &rhs) const {
    return !(rhs == *this);
}


MoleculeComponent& MoleculeComponent::operator=(const MoleculeComponent &rhs) {

    if (simple)
        delete (Element*) component;
    else
        delete (Molecule*) component;

    if (rhs.isElement())
    {
        component = new Element(rhs.getElement());
        simple = true;
    }
    else if (rhs.isMolecule())
    {
        component = new Molecule(rhs.getMolecule());
        simple = false;
    }

    return *this;
}



