#include <boost/units/quantity.hpp>
#include <boost/units/systems/detail/constants.hpp>
#include <boost/units/systems/si/codata/electromagnetic_constants.hpp>
#include <boost/units/systems/si/codata/electron_constants.hpp>
#include <ostream>

#include "zmoles/IonSpecie.hpp"
#include "zmoles/Utils.hpp"
#include "zmoles/ZMolesError.hpp"

using namespace dfpe;
using namespace std;
using namespace boost::units;

const string IonSpecie::getState()  const
{
	return state;
}

IonSpecie& IonSpecie::operator=(const IonSpecie &rhs)
{
	molecule 	= rhs.molecule;
	ionization  = rhs.ionization;
	state	 	= rhs.state;
	
	return *this;
}

bool IonSpecie::similar(const IonSpecie &rhs, bool allowStateMismatch, bool allowIonizationMismatch) const
{
	return getMolecule()     == rhs.getMolecule()  &&
           ((allowIonizationMismatch && rhs.getIonization() == 0) || getIonization()   == rhs.getIonization()) &&
           ((allowStateMismatch      && rhs.getState().empty())   || getState()        == rhs.getState());
}

bool IonSpecie::operator==(const IonSpecie &rhs) const
{
    return getMolecule()     == rhs.getMolecule()  &&
           getIonization()   == rhs.getIonization() &&
           getState()        == rhs.getState();
}

bool  IonSpecie::operator!=(const IonSpecie &rhs) const
{
    return !(rhs == *this);
}

const string IonSpecie::toString(const PrintMode& mode) const
{
    if (mode == PrintMode::BASIC)
		return toBasicString();
    else if (mode == PrintMode::PRETTY)
		return toPrettyString();
    else if (mode == PrintMode::LATEX)
		return toLatexString();
    else
	throw ZMolesError("Unable to decode the provided PrintMode. Contact developers.");
}

const string IonSpecie::toBasicString() const
{
    stringstream ss;

    ss << molecule.toString(PrintMode::BASIC);

    string ionizationStr;

    if (ionization > 0)
    {
		for (int i = 0; i < ionization; i++)
			ionizationStr += "^+";
	}
    else if (ionization < 0)
    {
		for (int i = 0; i > ionization; i--)
        ionizationStr += "^-";
	}

    ss << ionizationStr;

    if (!state.empty())
        ss << "(" << state << ")";
    
    return ss.str();
}

const string IonSpecie::toPrettyString() const
{

	stringstream ss;
	
    ss << molecule.toString(PrintMode::PRETTY);
    
    string ionizationStr;

	if (ionization < -1 || ionization > 1)
		ionizationStr += Utils::getScriptedNumber(ionization, true, false);

    if (ionization > 0)
        ionizationStr += "⁺";
    else if (ionization < 0)
        ionizationStr += "⁻";

    ss << ionizationStr;

    if (!state.empty())
        ss << "(" << state << ")";

    return ss.str();
}

const string IonSpecie::toLatexString() const
{
    stringstream ss;
	
    ss << molecule.toString(PrintMode::LATEX);
    
    string ionizationStr;

    if (ionization < -1 || ionization > 1)
	ionizationStr += to_string(abs(ionization));

    if (ionization > 0)
	ionizationStr += "+";
    else if (ionization < 0)
	ionizationStr += "-";

    if (!ionizationStr.empty())
    {
	ionizationStr = "^{" + ionizationStr + "}";
    }

    ss << ionizationStr;

	if (!state.empty())
        ss << "\\left(" << state << "\\right)";

    return ss.str();
}

QtySiElectricCharge 	IonSpecie::getCharge() const
{
	return 1. * ionization * si::constants::codata::e;
}

int	IonSpecie::getIonization() const
{
	return ionization;
}

QtySiMass IonSpecie::getMass() const
{
    return molecule.getMass() -  1. * ionization * si::constants::codata::m_e;
}



IonSpecie& IonSpecie::operator+=(const IonSpecie& rhs)
{
    molecule   += rhs.molecule;
    ionization += rhs.ionization;
    
    if (!rhs.state.empty())
    {
	if (!state.empty())
	    state += ",";
	    
	state += rhs.state;
    }
    return *this;
}

IonSpecie& IonSpecie::operator+=(const ParticleSpecie&)
{
    ionization--;
    return *this;
}

IonSpecie& IonSpecie::operator-=(const ParticleSpecie&)
{
    ionization++;
    return *this;
}

IonSpecie dfpe::operator+(const IonSpecie& lhs, const IonSpecie& rhs)
{
    IonSpecie result(lhs);
    result += rhs;
    return result; 
}

IonSpecie dfpe::operator+(const IonSpecie& lhs, const ParticleSpecie& rhs)
{
    IonSpecie result(lhs);
    result += rhs;
    return result; 
}

IonSpecie dfpe::operator+(const ParticleSpecie& lhs, const IonSpecie& rhs)
{
    IonSpecie result(rhs);
    result += lhs;
    return result; 
}


IonSpecie dfpe::operator-(IonSpecie const & lhs, ParticleSpecie const & rhs)
{
    IonSpecie result(lhs);
    result -= rhs;
    return result;     
}

bool IonSpecie::operator<(const IonSpecie &rhs) const
{
	if (molecule != rhs.molecule)
		return molecule < rhs.molecule;
	if (ionization != rhs.ionization)
		return ionization < rhs.ionization;
	if (state != rhs.state)
		return state < rhs.state;
	return false;
}


ostream &operator<<(ostream &os, const IonSpecie &specie)
{
    os << specie.toString(PrintMode::BASIC);
    return os;
}
