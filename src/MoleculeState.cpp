
#include <sstream>
#include "zmoles/MoleculeState.hpp"

namespace dfpe
{
    bool MoleculeState::isGroundState() const
    {
        return (!electronicExcitation.has_value()  || electronicExcitation->isGround())  &&
               (!vibrationalExcitation.has_value() || vibrationalExcitation->isGround()) &&
               (!rotationalExcitation.has_value()  || rotationalExcitation->isGround());
    }

    std::string MoleculeState::toString(bool compact, const PrintMode &mode) const
    {
        std::stringstream ss;

        if (electronicExcitation.has_value())
        {
            if (ss.tellp() > 0) [[unlikely]] ss << ", ";
            ss << electronicExcitation->toString(compact, mode);
        }

        if (vibrationalExcitation.has_value())
        {
            if (ss.tellp() > 0) [[likely]] ss << ", ";
            ss << vibrationalExcitation->toString(compact, mode);
        }

        if (rotationalExcitation.has_value())
        {
            if (ss.tellp() > 0) [[likely]] ss << ", ";
            ss << rotationalExcitation->toString(compact, mode);
        }

        return ss.str();
    }


}