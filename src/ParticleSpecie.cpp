#include <boost/units/systems/detail/constants.hpp>
#include <boost/units/systems/si/codata/electromagnetic_constants.hpp>
#include <boost/units/systems/si/codata/electron_constants.hpp>

#include "zmoles/ParticleSpecie.hpp"

using namespace dfpe;
using namespace std;
using namespace boost::units;

int ParticleSpecie::getId()     const
{
    return id;
}

const std::string& ParticleSpecie::getName()   const
{
    return name;
}

QtySiMass  ParticleSpecie::getMass()   const
{
	return mass;
}

QtySiElectricCharge	ParticleSpecie::getCharge() const
{
	return (double) ionization * si::constants::codata::e;
}

int	ParticleSpecie::getIonization() const
{
	return ionization;
}

const string ParticleSpecie::toString(const PrintMode&) const
{
    //TODO: print latex names (be aware of anti-particles)
    return name;
}


bool ParticleSpecie::operator==(const ParticleSpecie & other) const
{
	return id == other.id;
}

bool ParticleSpecie::operator!=(const ParticleSpecie & other) const
{
	return id != other.id;
}

bool ParticleSpecie::operator<(const ParticleSpecie& other) const
{
	return id < other.id;
}

ostream &operator<<(ostream &os, const ParticleSpecie &specie)
{
	os << specie.toString(PrintMode::BASIC);
	return os;
}
