#include <boost/regex.hpp>
#include "zmoles/Utils.hpp"

using namespace std;
using namespace dfpe;

const string Utils::getScriptedNumber(int number, bool superscript, bool forcePlus)
{

    const vector<string> digitsSuper = {"⁰","¹","²","³","⁴","⁵","⁶","⁷","⁸","⁹"};
    const vector<string> digitsSub   = {"₀","₁","₂","₃","₄","₅","₆","₇","₈","₉"};

    string s;

    if (number < 0)
    {
        if (superscript)
            s += "⁻";
        else
            s += "₋";
    }
    else if (number > 0 and forcePlus)
    {
        if (superscript)
            s += "⁺";
        else
            s += "₊";
    }

    while (number > 0)
    {
        if (superscript)
            s = digitsSuper[number % 10] + s;
        else
            s = digitsSub[number % 10] + s;

        number /= 10;
    }

    return s;
}

const std::string Utils::formatElementPretty(const smatch &what)
{
    return what[1].str() + getScriptedNumber(stoi(what[2]), false);
}

const std::string Utils::formatElementLatex(const smatch &what)
{
    return what[1].str() + what[2].str();
}

const regex Utils::elementRegex         = regex( "([A-Z][a-z]{0,2})([0-9]*)");
const regex Utils::isomerRegex          = regex( "^([a-z0-9])-");
