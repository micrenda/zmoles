
#include "zmoles/ElectronicExcitation.hpp"
#include "zmoles/ZMolesError.hpp"
#include "zmoles/Molecule.hpp"

#include "../../zmoles/include/zmoles/Utils.hpp"

namespace dfpe
{
    const static std::vector<std::string> ORDINAL_UPPER_LETTERS    = {"X",   "A",  "C",   "D",   "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P"};
    const static std::vector<std::string> ORDINAL_LOWER_LETTERS    = {"x",   "a",  "c",   "d",   "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p"};

    const static std::vector<std::string> ATOMIC_LETTERS           = {"S",    "P",  "D",   "F",   "G",   "H",   "I",  "K", "L", "M", "N", "O", "Q", "R", "T", "U", "V", "W", "X", "Y", "Z"};
    const static std::vector<std::string> MOLECULAR_LETTERS_BASIC  = { "SIG", "PI", "DEL", "PHI", "GAM", "ETA", "IOT",  };
    const static std::vector<std::string> MOLECULAR_LETTERS_PRETTY = { "Σ",   "Π",  "Δ",   "Φ",   "Γ",   "Η",   "Ι" };
    const static std::vector<std::string> MOLECULAR_LETTERS_LATEX  = { "\\Sigma",   "\\Pi",  "\\Delta",   "\\Phi",   "\\Gamma" };

    ElectronicExcitation::ElectronicExcitation(const Molecule *molecule) : BaseExcitation(molecule) {}

    bool ElectronicExcitation::isGround() const
    {
        return false;
    }

    bool ElectronicExcitation::isSinglet() const
    {
        return getMultiplicity() == 1;
    }

    bool ElectronicExcitation::isDoublet() const
    {
        return getMultiplicity() == 2;
    }

    bool ElectronicExcitation::isTriplet() const
    {
        return getMultiplicity() == 3;
    }

    bool ElectronicExcitation::isQuartet() const
    {
        return getMultiplicity() == 4;
    }

    bool ElectronicExcitation::isQuintet() const
    {
        return getMultiplicity() == 5;
    }

    void ElectronicExcitation::validate() const
    {
        if (s.denominator() != 1 && s.denominator() != 2)
            throw ZMolesError("Spin quantum number must be integer or half-integer");
        if (j.denominator() != 1 && j.denominator() != 2)
            throw ZMolesError("Total orbital quantum number must be integer or half-integer");

    }

    std::string ElectronicExcitation::toString(bool compact, const PrintMode &mode) const
    {
        std::stringstream ss;

        if (mode == PrintMode::BASIC || mode == PrintMode::PRETTY)
        {
            if (compact || ordinal != 0)
            {
                ss << (ordinalSpinMatch ? ORDINAL_UPPER_LETTERS[ordinal] : ORDINAL_LOWER_LETTERS[ordinal]);
            }

            ss << getMultiplicity();
            if (molecule->isMonoatomic())
                ss <<  ATOMIC_LETTERS[l];
            else
                ss << (mode == PrintMode::PRETTY ? MOLECULAR_LETTERS_PRETTY[l]  : MOLECULAR_LETTERS_BASIC[l]);
            ss << j;

            bool spaced = false;

            if (parity.has_value())
            {
                if (!spaced)
                {
                    ss << " ";
                    spaced = true;
                }

                ss << (*parity == ParityInversionType::EVEN ? "u" : "g");
            }

            if (reflection.has_value())
            {
                if (!spaced)
                {
                    ss << " ";
                    spaced = true;
                }

                ss << (*reflection == ReflectionSymmetryType::SYMMETRIC ? "+" : "-");
            }
        }
        else if (mode == PrintMode::LATEX)
        {
            if (compact || ordinal != 0)
            {
                if (!molecule->isMonoatomic() && !molecule->isDiatomic())
                    ss << "\\tilde{";

                ss << (ordinalSpinMatch ? ORDINAL_UPPER_LETTERS[ordinal] : ORDINAL_LOWER_LETTERS[ordinal]);

                if (!molecule->isMonoatomic() && !molecule->isDiatomic())
                    ss << "}";
            }

            ss << "\\prescript{" << getMultiplicity() << "}{}{";
            if (molecule->isMonoatomic())
                ss <<  ATOMIC_LETTERS[l];
            else
                ss << (mode == PrintMode::PRETTY ? MOLECULAR_LETTERS_PRETTY[l]  : MOLECULAR_LETTERS_BASIC[l]);
            ss << "}";

            if (reflection.has_value())
            {
                ss << "^{";
                ss << (*reflection == ReflectionSymmetryType::SYMMETRIC ? "+" : "-");
                ss << "}";
            }

            ss << "_{" << j;
            if (parity.has_value())
            {
                ss << ",";
                ss << (*parity == ParityInversionType::EVEN ? "u" : "g");
            }
            ss << "}";
        }
        else
        {
            throw ZMolesError("Unknown PrintMode: " + std::to_string((int) mode) );
        }

        return ss.str();
    }

    int ElectronicExcitation::getMultiplicity() const
    {
        boost::rational<int> buffer = 2 * s;
        assert(buffer.denominator() == 1);
        return buffer.numerator() + 1;
    }


} // dfpe