//
// Created by mrenda on 5/4/23.
//

#include "zmoles/VibrationalModeType.hpp"
#include "zmoles/ZMolesError.hpp"

namespace dfpe
{
    const std::string VibrationalModeTypeUtil::getName(const VibrationalModeType &type)
    {
        switch (type)
        {
            case VibrationalModeType::SYMMETRICAL_STRETCHING:
                return "SYMMETRICAL_STRETCHING";
            case VibrationalModeType::ASYMMETRICAL_STRETCHING:
                return "ASYMMETRICAL_STRETCHING";
            case VibrationalModeType::SCISSORING:
                return "SCISSORING";
            case VibrationalModeType::ROCKING:
                return "ROCKING";
            case VibrationalModeType::WAGGING:
                return "WAGGING";
            case VibrationalModeType::TWISTING:
                return "TWISTING";
            case VibrationalModeType::OUT_OF_PLANE:
                return "OUT_OF_PLANE";
            default:
                throw ZMolesError("Unknown vibrational mode type: " + std::to_string(static_cast<int>(type)));
        }
    }

    const std::string VibrationalModeTypeUtil::getSymb(const VibrationalModeType &type, PrintMode mode)
    {
        if (mode == PrintMode::BASIC)
        {
            switch (type)
            {
                case VibrationalModeType::SYMMETRICAL_STRETCHING:
                    return "SS";
                case VibrationalModeType::ASYMMETRICAL_STRETCHING:
                    return "AS";
                case VibrationalModeType::SCISSORING:
                    return "SC";
                case VibrationalModeType::ROCKING:
                    return "RO";
                case VibrationalModeType::WAGGING:
                    return "WA";
                case VibrationalModeType::TWISTING:
                    return "TW";
                case VibrationalModeType::OUT_OF_PLANE:
                    return "OP";
                default:
                    throw ZMolesError("Unknown vibrational mode type: " + std::to_string(static_cast<int>(type)));
            }
        }
        else if (mode == PrintMode::PRETTY)
        {
            switch (type)
            {
                case VibrationalModeType::SYMMETRICAL_STRETCHING:
                    return "νₛ";
                case VibrationalModeType::ASYMMETRICAL_STRETCHING:
                    return "νₐₛ";
                case VibrationalModeType::SCISSORING:
                    return "δ";
                case VibrationalModeType::ROCKING:
                    return "ρ";
                case VibrationalModeType::WAGGING:
                    return "ω";
                case VibrationalModeType::TWISTING:
                    return "τ";
                case VibrationalModeType::OUT_OF_PLANE:
                    return "γ";
                default:
                    throw ZMolesError("Unknown vibrational mode type: " + std::to_string(static_cast<int>(type)));
            }
        }
        else if (mode == PrintMode::LATEX)
        {
            switch (type)
            {
                case VibrationalModeType::SYMMETRICAL_STRETCHING:
                    return "\\nu_{s}";
                case VibrationalModeType::ASYMMETRICAL_STRETCHING:
                    return "\\nu_{as}";
                case VibrationalModeType::SCISSORING:
                    return "\\delta";
                case VibrationalModeType::ROCKING:
                    return "\\rho";
                case VibrationalModeType::WAGGING:
                    return "\\omega";
                case VibrationalModeType::TWISTING:
                    return "\\tau";
                case VibrationalModeType::OUT_OF_PLANE:
                    return "\\gamma";
                default:
                    throw ZMolesError("Unknown vibrational mode type: " + std::to_string(static_cast<int>(type)));
            }
        }
        else
        {
            throw ZMolesError("Unknown print mode: " + std::to_string(static_cast<int>(mode)));
        }
    }

    const std::string VibrationalModeTypeUtil::getDesc(const VibrationalModeType &type)
    {
        switch (type)
        {
            case VibrationalModeType::SYMMETRICAL_STRETCHING:
                return "Symmetrical stretching";
            case VibrationalModeType::ASYMMETRICAL_STRETCHING:
                return "Asymmetrical stretching";
            case VibrationalModeType::SCISSORING:
                return "Scissoring";
            case VibrationalModeType::ROCKING:
                return "Rocking";
            case VibrationalModeType::WAGGING:
                return "Wagging";
            case VibrationalModeType::TWISTING:
                return "Twisting";
            case VibrationalModeType::OUT_OF_PLANE:
                return "Out of plane";
            default:
                throw ZMolesError("Unknown vibrational mode type: " + std::to_string(static_cast<int>(type)));
        }
    }

} // dfpe