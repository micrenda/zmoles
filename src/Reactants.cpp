
#include "zmoles/Reactants.hpp"

using namespace std;
using namespace dfpe;

const std::string Reactants::toString(const PrintMode &mode) const {

    return bullet.toString(mode) + " + " + target.toString(mode);
}

int Reactants::getElectronsCount() const {
    int reactantsElectrons = 0;

    if (bullet.isParticle())
        reactantsElectrons++;
    if (target.isParticle())
        reactantsElectrons++;

    return reactantsElectrons;
}

int Reactants::getIonsCount() const {
    int reactantsIons = 0;

    if (bullet.isIon())
        reactantsIons++;
    if (target.isIon())
        reactantsIons++;

    return reactantsIons;
}

int Reactants::getSpecieCount(const Specie specie, bool ignoreCharge, bool ignoreState) const {
    int reactantsSpecies = 0;

    for (const Specie &reactant: {bullet, target})
    {
        if (specie.isParticle() && reactant.isParticle())
            reactantsSpecies++;
        else if (specie.isIon() && reactant.isIon())
        {
            const IonSpecie& specieIon   = specie.getIon();
            const IonSpecie& reactantIon = reactant.getIon();

            bool equal = specieIon.getMolecule() == reactantIon.getMolecule();

            equal &= ignoreCharge || specieIon.getCharge() == reactantIon.getCharge();
            equal &= ignoreState || specieIon.getState() == reactantIon.getState();

            if (equal)
                reactantsSpecies++;
        }
    }

    return reactantsSpecies;
}
