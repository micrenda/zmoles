#include <sstream>
#include "zmoles/RotationalExcitation.hpp"

namespace dfpe
{
    RotationalExcitation::RotationalExcitation(const Molecule *molecule) : BaseExcitation(molecule) {}
    RotationalExcitation::RotationalExcitation(const Molecule *molecule, int j) : BaseExcitation(molecule), j(j) {}
    RotationalExcitation::RotationalExcitation(const Molecule *molecule, int j, int ka) : BaseExcitation(molecule), j(j), ka(ka) {}
    RotationalExcitation::RotationalExcitation(const Molecule *molecule, int j, int ka, int kc) : BaseExcitation(molecule), j(j), ka(ka), kc(kc) {}

    int RotationalExcitation::getJ() const
    {
        return j;
    }

    int RotationalExcitation::getKa() const
    {
        return ka;
    }

    int RotationalExcitation::getKc() const
    {
        return kc;
    }

    bool RotationalExcitation::isGround() const
    {
        return j == 0 && ka == 0 && kc == 0;
    }

    void RotationalExcitation::validate() const
    {

    }

    std::string RotationalExcitation::toString(bool compact, const PrintMode &mode) const
    {
        std::stringstream ss;

        if (!isGround() || !compact)
        {
            if (ss.tellp() > 0) [[unlikely]] ss << ", ";
            ss << "J=" << j;

            if (ka != 0)
            {
                if (ss.tellp() > 0) [[likely]] ss << ", ";
                ss << "Ka=" << ka;
            }

            if (kc != 0)
            {
                if (ss.tellp() > 0) [[likely]] ss << ", ";
                ss << "Kc=" << kc;
            }
        }

        return ss.str();
    }

} // dfpe