
#include <zmoles/ElectronSpecie.hpp>
#include "zmoles/Products.hpp"

using namespace std;
using namespace dfpe;

const std::string Products::toString(const PrintMode &mode) const
{
    std::stringstream      ss;
    bool first = true;
    int electrons = 0;
    for (const auto& item: items)
    {
        if (item.isIon())
        {
            if (!first)
                ss << " + ";
            ss << item.toString(mode);
            first = false;
        }
        else if(item.isParticle())
        {
            electrons++;
        }
    }

    if (electrons > 0)
    {
        if (!first)
            ss << " + ";
        if (electrons > 1)
            ss << electrons;
        ss << Specie(ElectronSpecie());
        first = false;
    }

    return ss.str();
}

void Products::clear() { items.clear();}

bool Products::empty() const { return items.empty(); }

void Products::append(const Specie &specie) {
    items.push_back(specie);
}

int Products::getIonsCount() const {
    int productsIons  = 0;

    for (const Specie &item: items)
    {
        if (item.isIon())
            productsIons++;
    }
    return productsIons;
}

int Products::getElectronsCount() const {
    int productsElectrons  = 0;

    for (const Specie &item: items)
    {
        if (item.isParticle())
            productsElectrons++;
    }
    return productsElectrons;
}

int Products::getSpecieCount(const Specie specie, bool ignoreCharge, bool ignoreState) const {
    int productsSpecies = 0;

    for (const Specie &item: items)
    {
        if (specie.isParticle() && item.isParticle())
            productsSpecies++;
        else if (specie.isIon() && item.isIon())
        {
            const IonSpecie& specieIon   = specie.getIon();
            const IonSpecie& productIon = item.getIon();

            bool equal = specieIon.getMolecule() == productIon.getMolecule();

            equal &= ignoreCharge || specieIon.getCharge() == productIon.getCharge();
            equal &= ignoreState || specieIon.getState() == productIon.getState();

            if (equal)
                productsSpecies++;
        }
    }

    return productsSpecies;
}

